module.exports = function(eleventyConfig) {
	eleventyConfig.addPassthroughCopy("src/_www");

  return {
    dir: { input: 'src', output: '_site' },
    passthroughFileCopy: true
  };
};
